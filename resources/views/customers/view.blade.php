@extends('layouts.app')

@section('content')
    <div class="row well">
        <div class="col-lg-7">
            <h2>{{$customers->fantasy_name}}</h2>
            <p>Razão Social: {{$customers->company_name}}</p>
            <p>CNPJ: {{$customers->cnpj}}</p>
            <p>Email: {{$customers->email}}</p>
            <p>Telefone: {{$customers->phone}}</p>
            <p>Cidade: {{$customers->city}}</p>
            <p>Bairro: {{$customers->district}}</p>
            <p>Estado: {{$customers->state}}</p>
            <p>Endereço: {{$customers->address}}</p>
            <p>CEP: {{$customers->zip_code}}</p>
        </div>
    </div>
@stop
