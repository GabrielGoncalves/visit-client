@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">Clientes</div>

                <div class="panel-body">
                    <a href="{{ url('/customers/create') }}" class="btn btn-success">Novo Cliente</a>
                    <table class="table table-striped">
                    
                        <thead>
                            <tr>
                                <td> ID </td>
                                <td> Razao Social </td>
                                <td> Cidade </td>
                                <td> Bairro </td>
                                <td> Telefone </td>
                                <td> Options </td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $customer)
                            <tr>
                                <td> {{$customer->id}}</td>
                                <td> {{$customer->fantasy_name}}</td>
                                <td> {{$customer->city}}</td>
                                <td> {{$customer->district}}</td>
                                <td> {{$customer->phone}}</td>
                                <td>
                                    {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'DELETE'])!!}
                                        <a href="{{route('customers.edit', [$customer->id])}}" title="Edit"><i class="fa fa-pencil fa-fw"></i></a>
                                        <a href="{{route('customers.show', [$customer->id])}}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <button type="submit" title="Delete" class="btn-link"><i class="fa fa-trash-o fa-fw"></i></button>
                                    {!! Form::close()!!}

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $customers->render()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

