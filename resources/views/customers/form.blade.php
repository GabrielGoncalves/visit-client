<?php
use App\Helpers\StatesHelper;
?>

<div class="form-group {{ $errors->has('fantasy_name') ? ' has-error' : '' }}">
    {!!Form::label('fantasy_name', 'Nome Fantasia', [])!!}
    {!!Form::text('fantasy_name', null, ['class' => "form-control", 'alt'=>"Nome Fantasia"])!!}
    @if ($errors->has('fantasy_name'))
        <span class="help-block">
            <strong>{{ $errors->first('fantasy_name') }}</strong>
        </span>
    @endif

</div>
<div class="form-group {{ $errors->has('company_name') ? ' has-error' : '' }}">
    {!!Form::label('company_name', 'Razão Social', [])!!}
    {!!Form::text('company_name', null, ['class' => "form-control", 'alt'=>"Razão Social"])!!}
    @if ($errors->has('company_name'))
        <span class="help-block">
            <strong>{{ $errors->first('company_name') }}</strong>
        </span>
    @endif

</div>
<div class="form-group {{ $errors->has('cnpj') ? ' has-error' : '' }}">
    {!!Form::label('cnpj', null, [])!!}
    {!!Form::text('cnpj', null,['class' => "form-control", 'alt' => "CNPJ"])!!}
    @if ($errors->has('cnpj'))
        <span class="help-block">
            <strong>{{ $errors->first('cnpj') }}</strong>
        </span>
    @endif

</div>
<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    {!!Form::label('email', null, [])!!} 
    {!!Form::email('email', null, ['class' => "form-control", 'alt'=>"E-mail"])!!}
    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
    {!!Form::label('phone', 'Telefone', [])!!}
    {!!Form::text('phone', null, ['class' => "form-control", 'alt' => "Telefone"])!!}
    @if ($errors->has('phone'))
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
    {!!Form::label('city', 'Cidade', [])!!}
    {!!Form::select('city', array('01' => 'Niterói', '02' => 'São Gonçalo'), null, ['placeholder' => 'Selecione a cidade...', 'class' => "form-control"])!!}
    @if ($errors->has('city'))
        <span class="help-block">
            <strong>{{ $errors->first('city') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('district') ? ' has-error' : '' }}">
    {!!Form::label('district', 'Bairro', [])!!}
    {!!Form::text('district', null, ['class' => "form-control", 'alt' => "Bairro"])!!}
    @if ($errors->has('district'))
        <span class="help-block">
            <strong>{{ $errors->first('district') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
    {!!Form::label('state', 'Estado', [])!!}
    {!!Form::select('state', StatesHelper::renderAll(), null, ['placeholder' => 'Selecione um estado...', 'class' => "form-control"])!!}
    @if ($errors->has('state'))
        <span class="help-block">
            <strong>{{ $errors->first('state') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
    {!!Form::label('address', 'Endereço', [])!!}
    {!!Form::text('address', null, ['class' => "form-control", 'alt' => "Endereço"])!!}
    @if ($errors->has('address'))
        <span class="help-block">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('zip_code') ? ' has-error' : '' }}">
    {!!Form::label('zip_code', 'CEP', [])!!}
    {!!Form::text('zip_code', null, ['class' => "form-control", 'alt' => "CEP"])!!}
    @if ($errors->has('zip_code'))
        <span class="help-block">
            <strong>{{ $errors->first('zip_code') }}</strong>
        </span>
    @endif

</div>

