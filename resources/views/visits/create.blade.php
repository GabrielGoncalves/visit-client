@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h2>Nova Visita</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-7">
                            {!!Form::open(['route'=>'visits.store', 'method'=>'POST'])!!}
                                @include('visits.form')
                                {!!Form::submit('Cadastrar', ['class' => "btn btn-lg btn-success"])!!}
                                <a href="{{ url()->previous() }}" class="btn btn-lg btn-danger">Back</a>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
