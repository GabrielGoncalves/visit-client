@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">Visitas</div>

                <div class="panel-body">
                    <a href="{{ url('/visits/create') }}" class="btn btn-success">Nova Visita</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td> ID </td>
                                <td> Cliente </td>
                                <td> Data da Visita </td>
                                <td> Qtde. Vendida </td>
                                <td> Valor Recebido </td>
                                <td> Options </td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($visits as $visit)
                            <tr>
                                <td> {{$visit->id}}</td>
                                <td> {{$visit->customer->fantasy_name}}</td>
                                <td> {{$visit->present()->visitDate}}</td>
                                <td> {{$visit->sales_quantity}}</td>
                                <td> {{$visit->present()->valueReceived}}</td>
                                <td>
                                    {!! Form::open(['route' => ['visits.destroy', $visit->id], 'method' => 'DELETE'])!!}
                                        <a href="{{route('visits.edit', [$visit->id])}}" title="Edit"><i class="fa fa-pencil fa-fw"></i></a>
                                        <a href="{{route('visits.show', [$visit->id])}}" title="View"><i class="fa fa-eye fa-fw"></i></a>
                                        <button type="submit" title="Delete" class="btn-link"><i class="fa fa-trash-o fa-fw"></i></button>
                                    {!! Form::close()!!}

                                </td>
                            </tr>
                        @endforeach  
                        </tbody>
                    </table>
                    <div class="text-center">
                        {!! $visits->render()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

