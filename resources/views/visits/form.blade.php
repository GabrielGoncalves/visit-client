<div class="form-group {{ $errors->has('customer_id') ? ' has-error' : '' }}">
    {!!Form::label('customer_id', 'Cliente', [])!!}
    {!!Form::select('customer_id', array('01' => 'Niterói', '02' => 'São Gonçalo'), null, ['placeholder' => 'Selecione o cliente...', 'class' => "form-control customers"])!!}
    @if ($errors->has('customer_id'))
        <span class="help-block">
            <strong>{{ $errors->first('customer_id') }}</strong>
        </span>
    @endif

</div>

<div class="form-group {{ $errors->has('visit_date') ? ' has-error' : '' }}">
    {!!Form::label('visit_date', 'Data da Visita', [])!!}
    <br>
    {!!Form::date('visit_date', \Carbon\Carbon::now())!!}
    @if ($errors->has('visit_date'))
        <span class="help-block">
            <strong>{{ $errors->first('visit_date') }}</strong>
        </span>
    @endif

</div>
<div class="form-group {{ $errors->has('sales_quantity') ? ' has-error' : '' }}">
    {!!Form::label('sales_quantity', 'Quantidade Vendida', [])!!}
    {!!Form::text('sales_quantity', null,['class' => "form-control", 'alt' => "Quantidade Vendida"])!!}
    @if ($errors->has('sales_quantity'))
        <span class="help-block">
            <strong>{{ $errors->first('sales_quantity') }}</strong>
        </span>
    @endif

</div>
<div class="form-group {{ $errors->has('value_received') ? ' has-error' : '' }}">
    {!!Form::label('value_received', 'Valor Recebido', [])!!} 
    <div class="input-group">
        <span class="input-group-addon">R$</span>
        {!!Form::text('value_received', null,['class' => "form-control value_received", 'alt' => "Valor Recebido"])!!}
    </div>
    @if ($errors->has('value_received'))
        <span class="help-block">
            <strong>{{ $errors->first('value_received') }}</strong>
        </span>
    @endif

</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(".customers").select2();
        $('.value_received').mask("000.000.000.000.000,00", {reverse: true, 
            onComplete: function(cep) {
                alert('CEP Completed!:' + cep);
            }
        });
    });
</script>
