@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h2>Visita</h2>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-7">
                            <h2>{{$visits->customer->fantasy_name}}</h2>
                            <p>Data : {{$visits->visit_date}}</p>
                            <p>Quantidade Vendida: {{$visits->sales_quantity}}</p>
                            <p>Valor Recebido: {{$visits->value_received}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
