<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Visits extends Model
{

    use PresentableTrait;

    protected $presenter = \App\Presenters\VisitsPresenter::class;

    protected $table = 'visits';
    protected $fillable = [
        'customer_id',
        'visit_date',
        'sales_quantity',
        'value_received'
    ];

    // Implementar Pattern Presenter
    public function setValueReceivedAttribute($value)
    {
        $this->attributes['value_received'] = str_replace(',', '', $value);
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers');
    }
}
