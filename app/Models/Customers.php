<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'fantasy_name', 'company_name', 'cnpj', 'email', 'phone', 'city', 'district', 'state', 'address', 'zip_code', 'excluded'
    ];
}
