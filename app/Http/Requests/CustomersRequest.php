<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fantasy_name' => 'required|max:255',
            'company_name' => 'string|max:255',
            'cnpj' => 'numeric',
            'email' => 'email|max:255|unique:users'
        ];
    }
}
