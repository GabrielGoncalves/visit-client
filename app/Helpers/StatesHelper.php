<?php

namespace App\Helpers;

use App\Models\States;

class StatesHelper
{

    public static function renderAll()
    {
        $states =  States::all()->toArray();

        $data = [];

        foreach ($states as $state) {
            $data[$state['id']] = $state['name'];
        }

        return $data;
    }

}
