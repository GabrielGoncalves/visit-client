<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class VisitsPresenter extends Presenter
{

    public function valueReceived()
    {
        return "R$ " . number_format($this->value_received, 2, ",", ".");
    }

    public function visitDate()
    {
        $date = new \Carbon\Carbon($this->visit_date);
        return $date->format('d/m/Y');
    }

}
